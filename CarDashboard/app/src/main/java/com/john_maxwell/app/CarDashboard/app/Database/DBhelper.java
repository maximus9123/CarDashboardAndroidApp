package com.john_maxwell.app.CarDashboard.app.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

/**
 * Created by John on 24/08/2017.
 */

public class DBhelper extends SQLiteOpenHelper {

    public DBhelper(Context context) {
        super(context, "Default_Device", null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create Table Default_Device (DefaultD Text, Current Text);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drop Table If Exists Default_Device;");
        onCreate(db);
    }

    public boolean insertDefaultDevice(String defaultD) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("DefaultD", defaultD);

        long res = db.insert("Default_Device", null, contentValues);
        if (res == -1)
            return false;
        else
            return true;
    }

    public void insertCurrentDevice(Context context, String current) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("INSERT INTO Default_Device (Current) Values ('" + current + "');");
        Toast.makeText(context, "Current updated with: " + current, Toast.LENGTH_SHORT).show();

        //ContentValues contentValues = new ContentValues();
        //contentValues.put("Current", current);


        /*long res = db.insert("Default_Device", null, contentValues);
        if (res == -1)
            return false;
        else
            return true;*/
    }

    public Cursor getDefaultData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cu = db.rawQuery("Select DefaultD From Default_Device;", null);
        return cu;
    }

    public Cursor getCurrentData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cu = db.rawQuery("Select Current From Default_Device;", null);
        return cu;
    }

    public void clrData() {
        SQLiteDatabase db = this.getWritableDatabase();

        if (db != null){
            db.execSQL("Drop Table If Exists Default_Device;");
            onCreate(db);
        }

    }

}






