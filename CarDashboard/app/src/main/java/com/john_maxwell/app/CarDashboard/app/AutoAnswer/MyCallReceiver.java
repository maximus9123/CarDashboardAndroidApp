package com.john_maxwell.app.CarDashboard.app.AutoAnswer;

/**
 * Created by John on 15/08/2017.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class MyCallReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            // This code will execute when the phone has an incoming call

            // get the phone number
            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

            if (incomingNumber == null){
                Toast.makeText(context, "Call from: Unknown" , Toast.LENGTH_LONG).show();
                answer(context);

                Log.d("MyTrack call", "call receive");
            }
            else{
                Toast.makeText(context, "Call from:" +incomingNumber, Toast.LENGTH_LONG).show();
                answer(context);
                Log.d("MyTrack call", "call receive");
            }
        }
    }

    public void answer(Context context) {
        //countdown timer to give time (10 secs) to decline call
        new CountDownTimer(10000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                try {
                    //Root permissions required
                    //simulate call button being pressed
                    //su - grants SuperUser Privileges
                    //-c - runs following line with Root privileges
                    //input keyevent 5 - programmatically presses answer button
                    Process process = Runtime.getRuntime().exec(new String[]{"su", "-c",
                            "input keyevent 5"});
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();

        Toast.makeText(context, "Call Auto Answered", Toast.LENGTH_LONG).show();
    }
}
