package com.john_maxwell.app.CarDashboard.app.GetCoordinates;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

/**
 * Created by John on 30/08/2017.
 */

public class GPSposition extends Service implements LocationListener {

    private final Context context;

    boolean isGPSEnabled = false;
    boolean canGetLocation = false;
    boolean isNetworkEnabled = false;

    double lat;
    double lon;

    Location location;

    private static final long MIN_DISTANCE_FOR_UPDATES = 10;
    private static final long TIME_BETWEEN_UPDATES = 60000;

    protected LocationManager locationManager;

    public GPSposition(Context context){
        this.context = context;
        getLocation();
    }

    public Location getLocation(){
        try {
            locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);

            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if(!isGPSEnabled == true && !isNetworkEnabled == true){
                Toast.makeText(context, "Error: Cannot get Location!", Toast.LENGTH_SHORT).show();
            }
            else{
                this.canGetLocation = true;

                if(isNetworkEnabled == true){
                    try {
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                                TIME_BETWEEN_UPDATES, MIN_DISTANCE_FOR_UPDATES, this);
                    }
                    catch (SecurityException se){
                        Toast.makeText(context, se.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    if (locationManager != null){
                        try {
                            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        }catch (SecurityException se){
                            Toast.makeText(context, se.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                        if (location != null){
                            lat = location.getLatitude();
                            lon = location.getLongitude();
                        }
                    }
                }

                if(isGPSEnabled == true){
                    if(location == null){
                        try {
                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                                    TIME_BETWEEN_UPDATES, MIN_DISTANCE_FOR_UPDATES, this);
                        }
                        catch (SecurityException se){
                            Toast.makeText(context, se.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        if(locationManager != null) {
                            try {
                                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            } catch (SecurityException se) {
                                Toast.makeText(context, se.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                            if (location != null) {
                                lat = location.getLatitude();
                                lon = location.getLongitude();
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return location;
    }

    public void stopUsingGPS(){
        if (locationManager != null){
            locationManager.removeUpdates(GPSposition.this);
        }
    }

    public double getLat(){
        if(location != null){
            lat = location.getLatitude();
        }
        return lat;
    }

    public double getLon() {
        if (location != null) {
            lon = location.getLongitude();
        }
        return lon;
    }

    public boolean canGetLocation(){
        return this.canGetLocation;
    }





    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
