package com.john_maxwell.app.CarDashboard.app.weather.data;

import org.json.JSONObject;

/**
 * Created by John on 20/07/2017.
 */

public interface JSONPopulator {
    void populate(JSONObject data);
}
