package com.john_maxwell.app.CarDashboard.app.ExitAppOnDiscconnect;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


/**
 * Created by John on 16/08/2017.
 */

public class ExitActivity extends Activity {

    @Override protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if(android.os.Build.VERSION.SDK_INT >= 21)
        {
            finishAndRemoveTask();//end previoius tasks and current task
        }
        else
        {
            finish();//end previoius tasks and current task
        }
    }

    public static void exitApplication(Context context)
    {
        Intent intent = new Intent(context, ExitActivity.class);
        //start the exit activity, clear all previous activities and prevent animations
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NO_ANIMATION );

        context.startActivity(intent);
    }
}