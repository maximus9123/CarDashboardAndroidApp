package com.john_maxwell.app.CarDashboard.app.Weather.service;

import com.john_maxwell.app.CarDashboard.app.Weather.data.Channel;

/**
 * Created by John on 20/07/2017.
 */

public interface WeatherServiceCallback {
    void serviceSuccess(Channel channel);
    void serviceFailure(Exception exception);

}
