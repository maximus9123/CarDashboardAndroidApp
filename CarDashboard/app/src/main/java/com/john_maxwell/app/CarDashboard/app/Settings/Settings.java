package com.john_maxwell.app.CarDashboard.app.Settings;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.john_maxwell.app.CarDashboard.R;
import com.john_maxwell.app.CarDashboard.app.Database.DBhelper;

public class Settings extends AppCompatActivity {


    private Button setBTDevice;
    private Button checkBTDevice;
    private Button getCurrent;

    DBhelper dBhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //initialise
        dBhelper = new DBhelper(this);

        //initialise Buttons
        checkBTDevice = (Button) findViewById(R.id.currentBTBtn);
        setBTDevice = (Button) findViewById(R.id.changeBTBtn);
        getCurrent = (Button) findViewById(R.id.getCur);

        //Initialise OnClickListeners
        getDefaultDevice();
        setSetBTDevice();
        getCurrentDevice();
    }

    public void getDefaultDevice() {
        checkBTDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor cu = dBhelper.getDefaultData();
                if (cu.getCount() == 0) {
                    Toast.makeText(Settings.this, "No Data", Toast.LENGTH_SHORT).show();
                    return;
                }
                cu.moveToFirst();
                String res = cu.getString(0);
                Toast.makeText(Settings.this, "Default Device is: " + res, Toast.LENGTH_SHORT).show();

            }
        });
    }
    public void getCurrentDevice() {
        getCurrent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor cu = dBhelper.getCurrentData();
                if (cu.getCount() == 0) {
                    Toast.makeText(Settings.this, "No Data", Toast.LENGTH_SHORT).show();
                    return;
                }
                cu.moveToFirst();
                String res = cu.getString(0);
                Toast.makeText(Settings.this, "Current Device is: " + res, Toast.LENGTH_SHORT).show();

            }
        });
    }


    public void setSetBTDevice() {
        setBTDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dBhelper.clrData();
                String res = null;


                Cursor cu = dBhelper.getCurrentData();
                if (cu == null && cu.getCount() == 0) {
                    Toast.makeText(Settings.this, "No Data", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(cu.getCount() > 0){
                    cu.moveToFirst();
                    res = cu.getString(0);
                }


                dBhelper.insertDefaultDevice(res);
                Toast.makeText(Settings.this, "Default Device Updated with: " + res, Toast.LENGTH_SHORT).show();
            }
        });

    }

}
