package com.john_maxwell.app.CarDashboard.app.RunOnStartup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.john_maxwell.app.CarDashboard.app.Bluetooth.OnBTConnect;

/**
 * Created by John on 15/08/2017.
 */

public class RunOnStartup extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        //intent to register BT broadcast listener on phone boot
        Intent i = new Intent(context, OnBTConnect.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }
}
