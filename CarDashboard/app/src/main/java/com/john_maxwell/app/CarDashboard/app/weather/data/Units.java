package com.john_maxwell.app.CarDashboard.app.weather.data;

import org.json.JSONObject;

/**
 * Created by John on 20/07/2017.
 */

public class Units implements JSONPopulator {
    private String temperature;

    @Override
    public void populate(JSONObject data) {
        temperature = data.optString("temperature");

    }

    public String getTemperature() {
        return temperature;
    }
}
