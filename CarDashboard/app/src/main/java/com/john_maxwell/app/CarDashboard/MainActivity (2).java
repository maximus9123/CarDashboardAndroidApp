package com.john_maxwell.app.CarDashboard;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.john_maxwell.app.CarDashboard.app.GetCoordinates.GPSposition;
import com.john_maxwell.app.CarDashboard.app.Settings.Settings;
import com.john_maxwell.app.CarDashboard.app.Time.Date;
import com.john_maxwell.app.CarDashboard.app.readSMS.SmsReciever;
import com.john_maxwell.app.CarDashboard.app.Weather.data.Channel;
import com.john_maxwell.app.CarDashboard.app.Weather.data.Item;
import com.john_maxwell.app.CarDashboard.app.Weather.service.WeatherServiceCallback;
import com.john_maxwell.app.CarDashboard.app.Weather.service.YahooWeatherService;

import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity implements WeatherServiceCallback, LocationListener, SensorEventListener {

    // variable declaration -->
    //permissions
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    //weather variables
    private ImageView weatherIconImageView;
    private TextView temperatureTextView;
    private TextView conditionTextView;
    private TextView locationTextView;
    private TextView locationTextView2;
    private YahooWeatherService service;
    private ProgressDialog dialog;

    //Compass Variables

    //Battery Variables
    private TextView batteryTextView;

    //SMS variables
    private Button smsButton;
    private String body = null;
    private String sender = null;
    private String name = null;

    private static final int PERMS_REQUEST_CODE = 123;  //Permissions Check Variable

    private SmsReciever smsReciever;  //SMS Broadcast Reciever

    private TextToSpeech tts;       //TTS declaration

    //compass variables
    private ImageView arrow;
    private float[] gravity = new float[3];
    private float[] geomagnetic = new float[3];
    private float azimuth = 0f;
    private float azimuthCorrect = 0f;
    private SensorManager sensorManager;

    //get Latitude Longitude variables
    private double lat;
    private double lon;
    GPSposition gps;

    //get address from lat/lon variable
    public String address;

    //BT settings variable
    public ImageButton settingsBTN;

    // <-- variable declaration


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.john_maxwell.app.CarDashboard.R.layout.activity_main);

        //initialise methods
        checkAndRequestPermissions();  //initialise permissions Check


        latLonToAddress();  //get Address name from lat/lon
        initTimeDate();  //Date and Time
        initBatteryLevel();  //battery level
        initSpeedo();  //Speedo
        initSMS();  //initialise SMS
        initCompass();  //initialise Compass
        initSettings(); //initialise BT Settings
        initWeatherService();  //initialise weather service



    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    //initialise methods -->

    //initialise weather method
    public void initWeather() {
        weatherIconImageView = (ImageView) findViewById(com.john_maxwell.app.CarDashboard.R.id.weatherIconImageView);
        temperatureTextView = (TextView) findViewById(com.john_maxwell.app.CarDashboard.R.id.temperatureTextView);
        conditionTextView = (TextView) findViewById(com.john_maxwell.app.CarDashboard.R.id.conditionTextView);
        locationTextView = (TextView) findViewById(com.john_maxwell.app.CarDashboard.R.id.LocationTextView);
        locationTextView2 = (TextView) findViewById(R.id.locationTV2);

        service = new YahooWeatherService(this);
        try {
            dialog = new ProgressDialog(this);
            dialog.setMessage("Loading...");
            dialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }

        String splitAddress[] = latLonToAddress().split(",");
        //location to be got from gps - API needed?
        service.refreshWeather(splitAddress[1] + "," + splitAddress[2]);


        //lat: "64.499474",long: "-165.405792
    }

    //initialise Time & Date Method
    public void initTimeDate() {
        TextView dateTextView = (TextView) findViewById(R.id.dateTextView);
        Date date = new Date();
        dateTextView.setText(date.getDateString());
    }

    //Initialise Battery Level Method
    public void initBatteryLevel() {

        batteryTextView = (TextView) findViewById(com.john_maxwell.app.CarDashboard.R.id.batteryTextView);
        getBatteryPercentage();
    }

    //Initialise Speedo
    public void initSpeedo() {
        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            checkAndRequestPermissions();
        }
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        this.onLocationChanged(null);
    }

    //initialise SMS
    public void initSMS() {
        final ImageButton readBtn = (ImageButton) findViewById(R.id.btnReadSMS);
        readBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSMS();
                readSMSAloud();
            }
        });
    }

    //initialise Compass
    public void initCompass() {
        arrow = (ImageView) findViewById(R.id.arrowImageView);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        //sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
    }
    //initialise BT Settings
    public void initSettings(){
        settingsBTN = (ImageButton) findViewById(R.id.settingsIBtn);
        settingsBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settingActivityIntent = new Intent(MainActivity.this, Settings.class);
                startActivity(settingActivityIntent);
            }
        });
    }
    //method to run weather service every 5 mins
    public void initWeatherService(){
        //handler with postDelayed to delay initial run to give time for address to be found and to
        //repeat after every 5 mins
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                initWeather();  // weather components
                handler.postDelayed(this, 300000); //update every 5 mins
            }
        }, 10000); //initial delay of 10 seconds
    }

    // <-- initialise methods
    /**/
    //weather methods -->
    //Weather update UI
    @Override
    public void serviceSuccess(Channel channel) {
        dialog.hide();

        Item item = channel.getItem();
        int resourceId = getResources().getIdentifier("@drawable/icon_" + item.getCondition().getCode(), null, getPackageName());

        //update weather icon
        Drawable weatherIconDrawable = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            weatherIconDrawable = getDrawable(resourceId);
        }

        weatherIconImageView.setImageDrawable(weatherIconDrawable);
        temperatureTextView.setText(item.getCondition().getTemperature() + "\u00B0" + channel.getUnits().getTemperature());
        conditionTextView.setText(item.getCondition().getDescription());
        String[] location = service.getLocation().split(",");
        locationTextView.setText(location[0]);
        locationTextView2.setText(location[1]);

    }

    //Weather - Failure
    @Override
    public void serviceFailure(Exception exception) {
        dialog.hide();
        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_LONG).show();

    }

    //<-- weather methods
    /**/
    //compass methods
    /**/
    //Speedo methods -->
    @Override
    public void onLocationChanged(Location location) {
        TextView speedoTextView = (TextView) this.findViewById(com.john_maxwell.app.CarDashboard.R.id.speedo);
        //check for null
        if (location == null) {
            speedoTextView.setText("-.- km/ h");
        } else {
            float currentSpeed = location.getSpeed();
            currentSpeed = currentSpeed * 3.6f;  //convert m/s to Km/h
            speedoTextView.setText(String.format("%.1f", currentSpeed) + " km/h");
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    // <-- Speedo methods
    /**/
    //battery methods -->
    public void getBatteryPercentage() {
        final BroadcastReceiver batteryInfoReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                context.unregisterReceiver(this);
                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 0);
                if (level > 0 && scale > 0) {
                    batteryTextView.setText(" " + ((level * 100) / 100) + "%");
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        this.registerReceiver(batteryInfoReciever, intentFilter);
    }

    // <-- battery methods
    /**/
    //Permissions methods -->
    //method to check permissions
    private boolean checkAndRequestPermissions() {
        int readSMS = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_SMS);
        int recieveSMS = ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS);
        int readContacts = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS);
        int accessFineLocation = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        int accessCoarseLocation = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int readPhoneState = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_SMS);
        }
        if (recieveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.RECEIVE_SMS);
        }
        if (accessFineLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_CONTACTS);
        }
        if (readContacts != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (accessCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (readPhoneState != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_PHONE_STATE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    // <-- Permissions methods
    /**/
    // Speak methods -->
    //method to speak text
    private void speak(String text) {
        //if/else for pre Lollipop
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    //method to stop TextToSpeach after exit
    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    public void pause() {

    }

    //read SMS message aloud
    public void readSMSAloud() {
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = tts.setLanguage(Locale.UK);
                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "This Language is not supported");
                    }
                    if (sender == null) {
                        speak("No new message");
                    }
                    //read number if unknown contact
                    else if (name == null) {
                        speak("Message From . . . " + sender + " . . . Messsage Reads: . . . " + body);
                    } else {
                        speak("Message From . . . " + name + " . . . Messsage Reads: . . . " + body);
                    }
                } else {
                    Log.e("TTS", "Initilization Failed!");
                }
            }
        });
    }

    // <-- Speak Methods
    /**/
    // get SMS info methods -->
    public void getSMS() {
        Uri sms_content = Uri.parse("content://sms/inbox");
        String where = "read = 0";
        Cursor c = this.getContentResolver().query(sms_content, null, where, null, null);
        if (c == null) {
            return;
        }
        try {
            c.moveToFirst();
            body = c.getString(c.getColumnIndexOrThrow("body"));
            sender = c.getString(c.getColumnIndexOrThrow("address"));
            name = getContactName(sender);
            c.close();
        } catch (Exception e) {
            Log.d("Error", "Cursor Empty");
        }
    }

    public String getContactName(String number) {
        String cName = null;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        String nameColumn[] = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
        Cursor c = getContentResolver().query(uri, nameColumn, null, null, null);
        if (c == null || c.getCount() == 0)
            return cName;
        c.moveToFirst();
        cName = c.getString(0);
        c.close();
        return cName;

    }

    // <-- get SMS info methods
    /**/
    //Compass Sensor methods -->
    @Override
    public void onSensorChanged(SensorEvent event) {
        final float alpha = 0.97f;
        synchronized (this) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
                gravity[1] = alpha * gravity[0] + (1 - alpha) * event.values[1];
                gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];
            }

            if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                geomagnetic[0] = alpha * geomagnetic[0] + (1 - alpha) * event.values[0];
                geomagnetic[1] = alpha * geomagnetic[0] + (1 - alpha) * event.values[1];
                geomagnetic[2] = alpha * geomagnetic[2] + (1 - alpha) * event.values[2];
            }

            float R[] = new float[9];
            float I[] = new float[9];

            boolean success = SensorManager.getRotationMatrix(R, I, gravity, geomagnetic);

            if (success = true) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                azimuth = (float) Math.toDegrees(orientation[0]);
                azimuth = (azimuth + 360) % 360;
            }
        }



        RotateAnimation rotateAnimation = new RotateAnimation(azimuthCorrect, -azimuth,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        rotateAnimation.setDuration(1000);

        rotateAnimation.setFillAfter(true);

        arrow.startAnimation(rotateAnimation);
        azimuthCorrect = -azimuth;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    // <-- Compass Sensor methods
    /**/
    //method to get Latitude & Longitude -->
    public void getLatLon() {
        gps = new GPSposition(MainActivity.this);

        if (gps.canGetLocation()) {
            lat = gps.getLat();
            lon = gps.getLon();
        }
    }

    // <-- method to get Latitude & Longitude
    //get address method  -->

    public String latLonToAddress() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        getLatLon();
        String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
                lat + "," + lon + "&key=AIzaSyASWAf9b-g-LYs0SrHPxEftXFP5nXhf_RE";
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            address = response.getJSONArray("results").getJSONObject(0).getString("formatted_address");
                            //Toast.makeText(MainActivity.this, "Text should be updated", Toast.LENGTH_SHORT).show();
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonObjectRequest);
        return address;
    }
        // <-- get address method
}
