package com.john_maxwell.app.CarDashboard.app.Time;

import java.text.SimpleDateFormat;

/**
 * Created by John on 23/07/2017.
 */

public class Date {

   private long date = System.currentTimeMillis();
    SimpleDateFormat simpleDF = new SimpleDateFormat("dd MMM yy");
    String dateString = simpleDF.format(date);

    public String getDateString() {
        return dateString;
    }
}
