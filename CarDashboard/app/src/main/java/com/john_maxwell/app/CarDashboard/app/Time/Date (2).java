package com.john_maxwell.app.CarDashboard.app.Time;

import java.text.SimpleDateFormat;

/**
 * Created by John on 23/07/2017.
 */

public class Date {

    //add current date and time data to variable date
    private long date = System.currentTimeMillis();
    //declare date format
    private SimpleDateFormat simpleDF = new SimpleDateFormat("dd MMM yy");
    //put current date into 'dateString' using simpleDF format
    private String dateString = simpleDF.format(date);

    //getter for date
    public String getDateString() {
        return dateString;
    }
}
