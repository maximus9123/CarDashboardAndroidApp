package com.john_maxwell.app.CarDashboard.app.Bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

import com.john_maxwell.app.CarDashboard.app.Database.DBhelper;
import com.john_maxwell.app.CarDashboard.app.ExitAppOnDiscconnect.ExitActivity;

/**
 * Created by John on 15/08/2017.
 */

public class OnBTConnect extends BroadcastReceiver {

    DBhelper dBhelper;


    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        Log.d("BroadcastActions", "Action " + action + "received");
        int state;
        BluetoothDevice bluetoothDevice;

        switch (action) {
            case BluetoothAdapter.ACTION_STATE_CHANGED:
                state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
                if (state == BluetoothAdapter.STATE_OFF) {
                    Toast.makeText(context, "Bluetooth is off", Toast.LENGTH_SHORT).show();
                    Log.d("BroadcastActions", "Bluetooth is off");
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Toast.makeText(context, "Bluetooth is turning off", Toast.LENGTH_SHORT).show();
                    Log.d("BroadcastActions", "Bluetooth is turning off");
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.d("BroadcastActions", "Bluetooth is on");
                }
                break;

            case BluetoothDevice.ACTION_ACL_CONNECTED:
                bluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                Toast.makeText(context, "Connected to " + bluetoothDevice.getName(),
                        Toast.LENGTH_SHORT).show();

                //initialise
                dBhelper = new DBhelper(context);
                String current = bluetoothDevice.getName();
                Toast.makeText(context, "passing: "+ current , Toast.LENGTH_SHORT).show();
                dBhelper.insertCurrentDevice(context, current);

                Log.d("BroadcastActions", "Connected to " + bluetoothDevice.getName());

                //intent to launch the app
                PackageManager pm = context.getPackageManager();
                Intent launchIntent = pm.getLaunchIntentForPackage("com.john_maxwell.app.CarDashboard");
                context.startActivity(launchIntent);

                break;

            case BluetoothDevice.ACTION_ACL_DISCONNECTED:
                bluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                Toast.makeText(context, "Disconnected from " + bluetoothDevice.getName(),
                        Toast.LENGTH_SHORT).show();

                //intent to exit the app
                ExitActivity.exitApplication(context);

                break;
        }
    }

}