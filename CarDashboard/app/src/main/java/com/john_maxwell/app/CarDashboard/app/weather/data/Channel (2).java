package com.john_maxwell.app.CarDashboard.app.Weather.data;

import org.json.JSONObject;

/**
 * Created by John on 20/07/2017.
 */

public class Channel implements JSONPopulator {
    private Item item;
    private  Units units;

    //getter for Item
    public Item getItem() {
        return item;
    }

    //getter for Units
    public Units getUnits() {
        return units;
    }

    //populate the units and Item section
    @Override
    public void populate(JSONObject data) {
        units = new Units();
        units.populate(data.optJSONObject("units"));

        item = new Item();
        item.populate(data.optJSONObject("item"));

    }
}
