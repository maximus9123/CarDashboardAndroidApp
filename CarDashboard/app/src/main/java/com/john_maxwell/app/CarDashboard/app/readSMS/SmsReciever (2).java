package com.john_maxwell.app.CarDashboard.app.readSMS;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

/**
 * Created by John on 14/08/2017.
 */

public class SmsReciever extends BroadcastReceiver {
    private String senderNumber = null;
    private String senderMessage = null;

    @Override
    public void onReceive(Context context, Intent intent) {

        //populate bundle with SMS receiver intent data i.e. message data
        Bundle bundle = intent.getExtras();


        if (bundle != null) {
            //populate pdus with pdus (protocol discription unit) data from intent
            Object[] pdus = (Object[]) bundle.get("pdus");

            for (int i = 0; i < pdus.length; i++) {

                //get data inside the 'format' section
                String format = bundle.getString("format");

                //code needs to be modified for < API23
                //use the SMSMessage Object in android to store the data from format as an SMS
                SmsMessage sms = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    sms = SmsMessage.createFromPdu((byte[]) pdus[i], format);
                }

                senderNumber = sms.getDisplayOriginatingAddress();  //get originating address i.e. phone number

                senderMessage = sms.getDisplayMessageBody();        //get message body

                Toast.makeText(context, "From: " + senderNumber + "Message: " + senderMessage, Toast.LENGTH_SHORT).show();

            }
        }
    }

}
