package com.john_maxwell.app.CarDashboard.app.readSMS;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

/**
 * Created by John on 14/08/2017.
 */

public class SmsReciever extends BroadcastReceiver {
    private String senderNumber = null;
    private String senderMessage = null;

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle bundle = intent.getExtras();


        if (bundle != null) {
            Object[] pdus = (Object[]) bundle.get("pdus");

            for (int i = 0; i < pdus.length; i++) {

                String format = bundle.getString("format");

                //code needs to be modified for < API23
                SmsMessage sms = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    sms = SmsMessage.createFromPdu((byte[]) pdus[i], format);
                }

                senderNumber = sms.getDisplayOriginatingAddress();

                senderMessage = sms.getDisplayMessageBody();

                Toast.makeText(context, "From: " + senderNumber + "Message: " + senderMessage, Toast.LENGTH_SHORT).show();

            }

            //SmsManager smsManager = SmsManager.getDefault();
            //smsManager.sendTextMessage(senderNumber, null, "Sorry I am currently driving, I will reply to you later!", null, null);
        }
    }

}
